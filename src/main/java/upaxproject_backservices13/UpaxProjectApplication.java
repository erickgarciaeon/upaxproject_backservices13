package upaxproject_backservices13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpaxProjectApplication {
	
	public static void main(String [] args) {
		SpringApplication.run(UpaxProjectApplication.class, args);
	}
}
