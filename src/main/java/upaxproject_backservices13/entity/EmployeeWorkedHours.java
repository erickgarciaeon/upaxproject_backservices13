package upaxproject_backservices13.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "EmployeesWorkedHours")
public class EmployeeWorkedHours {

	private int idWorkedHours;
	private int idEmployee;
	private int workedHours;
	private Date workedDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idWorkedHours")
	public Integer getIdWorkedHours() {
		return idWorkedHours;
	}
	public void setIdWorkedHours(int idWorkedHours) {
		this.idWorkedHours = idWorkedHours;
	}
	
	@OneToOne
	@Column(name = "idEmployee")
	public Integer getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}
	
	@Column(name = "workedHours")
	public Integer getWorkedHours() {
		return workedHours;
	}
	public void setWorkedHours(int workedHours) {
		this.workedHours = workedHours;
	}
	
	@Column(name = "workedDate")
	public Date getWorkedDate() {
		return workedDate;
	}
	public void setWorkedDate(Date workedDate) {
		this.workedDate = workedDate;
	}
	
	
}
