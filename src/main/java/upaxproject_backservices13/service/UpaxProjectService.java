package upaxproject_backservices13.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import upaxproject_backservices13.entity.EmployeeWorkedHours;
import upaxproject_backservices13.entity.Employees;
import upaxproject_backservices13.entity.Genders;
import upaxproject_backservices13.entity.Jobs;
import upaxproject_backservices13.facade.UpaxProjectFacade;

public class UpaxProjectService {

	@Autowired
	UpaxProjectFacade facade;

	public Map<String, Object> agregarEmpleado(Employees empleado) {
		Map<String, Object> respuesta = new HashMap<>();

		Employees employee = facade.findByNameAndLastName(empleado);
		Genders genders = facade.findByGender(empleado.getIdGender());
		Jobs jobs = facade.findByJob(empleado.getIdJob());

		boolean mayorEdad = validarEdad(empleado.getBirthdate());

		if (employee == null && genders != null && jobs != null && mayorEdad) {
			try {
				facade.save(empleado);
				Employees nuevo = facade.findByNameAndLastName(empleado);
				respuesta.put("id", nuevo.getIdEmployee());
				respuesta.put("success", true);
			} catch (Exception e) {
				e.getMessage();
				respuesta.put("id", null);
				respuesta.put("success", false);
			}
			respuesta.put("id", "error");
			respuesta.put("success", false);
		}
		return respuesta;
	}

	private boolean validarEdad(Date birthdate) {
		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate nacimiento = LocalDate.parse((CharSequence) birthdate, formato);
		LocalDate hoy = LocalDate.now();
		Period edad = Period.between(nacimiento, hoy);
		if (edad.getYears() >= 18) {
			return true;
		} else {
			return false;
		}
	}

	public Map<String, Object> registrarHoras(EmployeeWorkedHours horas) {
		Map<String, Object> respuesta = new HashMap<>();

		Employees employee = facade.findEmpleadoById(horas.getIdEmployee());
		boolean registro = validarRegistro(horas.getIdEmployee(), horas.getWorkedDate());
		boolean fecha = validarFechaIngresada(horas.getWorkedDate());

		if (employee != null && (horas.getWorkedHours() <= 20) && registro && fecha) {
			try {
				facade.saveHours(horas);
				EmployeeWorkedHours nuevo = facade.findWorkedHoursByEmployee(horas.getIdEmployee(), horas.getWorkedDate());
				respuesta.put("id", nuevo.getIdEmployee());
				respuesta.put("success", true);
			} catch (Exception e) {
				e.getMessage();
				respuesta.put("id", null);
				respuesta.put("success", false);
			}
			respuesta.put("id", null);
			respuesta.put("success", false);
		}

		return respuesta;
	}

	private boolean validarFechaIngresada(Date workedDate) {
		Date hoy = new Date();
		if (workedDate.compareTo(hoy) > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean validarRegistro(int idEmployee, Date date) {
		EmployeeWorkedHours registro = facade.findRegistroHoras(idEmployee, date);
		if (registro == null) {
			return true;
		} else {
			return false;
		}

	}

	public Map<String, Object> consultarEmpleados(int job) {
		Map<String, Object> respuesta = new HashMap<>();

		Jobs jobs = facade.findJobById(job);

		if (jobs != null) {
			try {
				List<Employees> empleados = facade.findEmployeeByJob(job);
				respuesta.put("employees", empleados);
				respuesta.put("success", true);
			} catch (Exception e) {
				e.getMessage();
				respuesta.put("id", null);
				respuesta.put("success", false);
			}
			respuesta.put("id", null);
			respuesta.put("success", false);
		}
		return respuesta;
	}

	public Map<String, Object> consultarHorasEmpleado(Integer idEmpleado, Date fechaInicio, Date fechaFin) {
		Map<String, Object> respuesta = new HashMap<>();

		Employees employee = facade.findEmpleadoById(idEmpleado);
		boolean fecha = validarFecha(fechaInicio, fechaFin);

		if (employee != null && fecha) {
			try {
				List<EmployeeWorkedHours> horas = facade.findHoursAll().stream().filter(hora -> hora.getIdEmployee().equals(idEmpleado)).collect(Collectors.toList());
				horas = horas.stream().filter(item ->{
					try {
						if(fechaFin.compareTo(fechaInicio)> 0) {
							return true;
						}else {
							return false;
						}
						
					}catch(Exception e){
						e.getMessage();
						return false;
					}
				}).collect(Collectors.toList());
				
				Integer horasTrabajadas = 0;
				for(int i = 0; i < horas.size(); i++) {
					horasTrabajadas = horasTrabajadas + horas.get(i).getWorkedHours();
				}
				
				respuesta.put("total_worked_hours", horasTrabajadas);
				respuesta.put("success", true);
			} catch (Exception e) {
				e.getMessage();
				respuesta.put("id", null);
				respuesta.put("success", false);
			}
			respuesta.put("id", null);
			respuesta.put("success", false);
		}

		return respuesta;
	}

	public Map<String, Object> consultarPago(int idEmpleado, Date fechaInicio, Date fechaFin) {
		Map<String, Object> respuesta = new HashMap<>();

		Employees employee = facade.findEmpleadoById(idEmpleado);
		boolean fecha = validarFecha(fechaInicio, fechaFin);

		if (employee != null && fecha) {
			try {
				List<EmployeeWorkedHours> horas = facade.findHoursAll().stream().filter(hora -> hora.getIdEmployee().equals(idEmpleado)).collect(Collectors.toList());
				horas = horas.stream().filter(item ->{
					try {
						if(fechaFin.compareTo(fechaInicio)> 0) {
							return true;
						}else {
							return false;
						}
						
					}catch(Exception e){
						e.getMessage();
						return false;
					}
				}).collect(Collectors.toList());
				
				Integer horasTrabajadas = 0;
				for(int i = 0; i < horas.size(); i++) {
					horasTrabajadas = horasTrabajadas + horas.get(i).getWorkedHours();
				}
				
				Jobs job = facade.findByJob(employee.getIdJob());
				
				Double pago = horasTrabajadas * job.getSalary();
				respuesta.put("payment", pago);
				respuesta.put("success", true);
			} catch (Exception e) {
				e.getMessage();
				respuesta.put("id", null);
				respuesta.put("success", false);
			}
			respuesta.put("id", null);
			respuesta.put("success", false);
		}

		return respuesta;
	}

	private boolean validarFecha(Date fechaInicio, Date fechaFin) {
		if (fechaFin.compareTo(fechaInicio) > 0) {
			return true;
		} else {
			return false;
		}
	}

}
