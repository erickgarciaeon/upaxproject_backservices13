package upaxproject_backservices13.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import upaxproject_backservices13.entity.EmployeeWorkedHours;

public interface UpaxProjectWorkedHourRepository extends CrudRepository<EmployeeWorkedHours, Integer>{

	EmployeeWorkedHours findByIdAndDate(int idEmployee, Date date);

	List<EmployeeWorkedHours> findByIdEmployee(int idEmployee);

	EmployeeWorkedHours findByIdEmployeeAndDate(int idEmployee, Date date);

}
