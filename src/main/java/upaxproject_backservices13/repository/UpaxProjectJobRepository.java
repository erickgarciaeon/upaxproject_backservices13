package upaxproject_backservices13.repository;

import org.springframework.data.repository.CrudRepository;

import upaxproject_backservices13.entity.Jobs;

public interface UpaxProjectJobRepository extends CrudRepository<Jobs, Integer>{

}
