package upaxproject_backservices13.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import upaxproject_backservices13.entity.Employees;

@Repository
public interface UpaxProjectEmployeeRepository extends CrudRepository<Employees, Integer>{

	Employees findByNameAndLastName(String name, String lastName);

	List<Employees> findByIdJob(int job);

}
