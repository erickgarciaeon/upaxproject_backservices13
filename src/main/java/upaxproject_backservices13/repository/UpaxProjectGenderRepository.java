package upaxproject_backservices13.repository;

import org.springframework.data.repository.CrudRepository;

import upaxproject_backservices13.entity.Genders;

public interface UpaxProjectGenderRepository extends CrudRepository<Genders, Integer>{

}
