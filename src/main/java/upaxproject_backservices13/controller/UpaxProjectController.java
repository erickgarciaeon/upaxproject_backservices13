package upaxproject_backservices13.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import upaxproject_backservices13.entity.EmployeeWorkedHours;
import upaxproject_backservices13.entity.Employees;
import upaxproject_backservices13.service.UpaxProjectService;

@RestController
@RequestMapping("/prueba")
public class UpaxProjectController {

	@Autowired
	UpaxProjectService service;
	
	@PostMapping("/nuevoEmpleado")
	@ApiOperation("Agregar nuevo empleado")
	public ResponseEntity<?> agregarEmpleado(@RequestParam Employees empleado){
		return new ResponseEntity<>(service.agregarEmpleado(empleado), HttpStatus.OK);
	}
	
	@PostMapping("/registarHoras")
	@ApiOperation("Agregar horas de empleado")
	public ResponseEntity<?> registrarHoras(@RequestParam EmployeeWorkedHours horas){
		return new ResponseEntity<>(service.registrarHoras(horas), HttpStatus.OK);
	}
	
	@GetMapping("/consultarEmpleados")
	@ApiOperation("Consultar empleados por puesto")
	public ResponseEntity<?> consultarEmpleados(@RequestParam int job){
		return new ResponseEntity<>(service.consultarEmpleados(job), HttpStatus.OK);
	}
	
	@GetMapping("/consultarHoras")
	@ApiOperation("Consultar horas de empleado")
	public ResponseEntity<?> consultarHorasEmpleado(@RequestParam int idEmpleado, Date fechaInicio, Date fechaFin){
		return new ResponseEntity<>(service.consultarHorasEmpleado(idEmpleado, fechaInicio, fechaFin), HttpStatus.OK);
	}
	
	@GetMapping("/pagoEmpleado")
	@ApiOperation("Consultar pago a empleado")
	public ResponseEntity<?> consultarPago(@RequestParam int idEmpleado, Date fechaInicio, Date fechaFin){
		return new ResponseEntity<>(service.consultarPago(idEmpleado, fechaInicio, fechaFin), HttpStatus.OK);
	}
	
}
