package upaxproject_backservices13.facade;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import upaxproject_backservices13.entity.EmployeeWorkedHours;
import upaxproject_backservices13.entity.Employees;
import upaxproject_backservices13.entity.Genders;
import upaxproject_backservices13.entity.Jobs;
import upaxproject_backservices13.repository.UpaxProjectEmployeeRepository;
import upaxproject_backservices13.repository.UpaxProjectGenderRepository;
import upaxproject_backservices13.repository.UpaxProjectJobRepository;
import upaxproject_backservices13.repository.UpaxProjectWorkedHourRepository;

@Component
public class UpaxProjectFacade {
	
	@Autowired
	UpaxProjectEmployeeRepository employeeRepository;
	
	@Autowired
	UpaxProjectGenderRepository genderRepository;
	
	@Autowired
	UpaxProjectJobRepository jobRepository;
	
	@Autowired
	UpaxProjectWorkedHourRepository workedHourRepository;

	public Employees findByNameAndLastName(Employees employee) {
		return employeeRepository.findByNameAndLastName(employee.getName(), employee.getLastName());
	}

	public void save(Employees empleado) {
		employeeRepository.save(empleado);
		
	}

	public Genders findByGender(int idGender) {
		return genderRepository.findById(idGender).get();
	}

	public Jobs findByJob(int idJob) {
		return jobRepository.findById(idJob).get();
	}

	public Employees findEmpleadoById(int idEmployee) {
		return employeeRepository.findById(idEmployee).get();
	}

	public EmployeeWorkedHours findRegistroHoras(int idEmployee, Date date) {
		return workedHourRepository.findByIdAndDate(idEmployee, date);
	}

	public void saveHours(EmployeeWorkedHours horas) {
		workedHourRepository.save(horas);
		
	}

	public EmployeeWorkedHours findWorkedHoursByEmployee(int idEmployee, Date date) {
		return workedHourRepository.findByIdEmployeeAndDate(idEmployee, date);
	}

	public Jobs findJobById(int job) {
		return jobRepository.findById(job).get();
	}

	public List<Employees> findEmployeeByJob(int job) {
		return employeeRepository.findByIdJob(job);
	}

	public List<EmployeeWorkedHours> findHoursByIdEmployee(int idEmpleado) {
		return workedHourRepository.findByIdEmployee(idEmpleado);
	}

	public List<EmployeeWorkedHours> findHoursAll() {
		return (List<EmployeeWorkedHours>) workedHourRepository.findAll();
	}

	
	
}
